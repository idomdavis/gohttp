package renderer

import (
	"bytes"
	"fmt"
	"html/template"
	"net/http"

	"bitbucket.org/idomdavis/gohttp/conversation"
)

// Template provides a wrapper around template.Template allowing for ease of
// rendering, and caching of template files.
type Template struct {
	// Reload can be set to true to cause template files to be reloaded on each
	// call to Render. This is generally reserved for development.
	Reload bool

	// Files is the set of files that form this template. At least one file must
	// be specified.
	Files []string

	conversation.Error

	tmpl *template.Template
}

// Render the Template to the ResponseWriter using the given data d.
func (t *Template) Render(w http.ResponseWriter, d interface{}) {
	b := bytes.Buffer{}

	if err := t.load(); err != nil {
		t.Handle(err)
		t.Handle(conversation.Respond(w, http.StatusInternalServerError))
	} else if err := t.tmpl.Execute(&b, d); err != nil {
		t.Handle(err)
		t.Handle(conversation.Respond(w, http.StatusInternalServerError))
	} else {
		t.Handle(conversation.Reply(w, b.Bytes()))
	}
}

func (t *Template) load() error {
	if t.tmpl != nil && !t.Reload {
		return nil
	}

	tmpl, err := template.ParseFiles(t.Files...)

	if err != nil {
		return fmt.Errorf("template load error: %w", err)
	}

	t.tmpl = tmpl

	return nil
}
