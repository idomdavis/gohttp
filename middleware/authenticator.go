package middleware

import (
	"net/http"

	"bitbucket.org/idomdavis/gohttp/conversation"
	"bitbucket.org/idomdavis/gohttp/handler"
	"bitbucket.org/idomdavis/gohttp/session"
)

// Authenticator provides a middleware function for authenticating requests
// using a signatory. Errors are put on to the Errors channel.
type Authenticator struct {
	Signatory session.Signatory
	Deny      http.HandlerFunc

	conversation.Error
}

// Authenticate is middleware that will check a request is authenticated either
// via a JWT set in the Authorization header, or a secure cookie. Authenticated
// requests will have a session.Context type built from the claims stored in the
// authorisation token. This context is then stored in the session.
//
// Unauthenticated requests will be passed to the Deny handler function. This
// defaults to the Unauthorised handler which returns StatusForbidden. Errors
// are put onto the Errors channel if one is set.
func (a Authenticator) Authenticate(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if a.Deny == nil {
			a.Deny = handler.Unauthorised{}.Handle
		}

		if a.Signatory == nil {
			a.Deny(w, r)
			return
		}

		ctx, err := a.Signatory.Validate(r)

		if err != nil {
			a.Handle(err)
			a.Deny(w, r)

			return
		}

		if ctx == nil {
			a.Deny(w, r)
			return
		}

		session.Store(r, ctx)
		next.ServeHTTP(w, r)
		session.Delete(r)
	})
}
