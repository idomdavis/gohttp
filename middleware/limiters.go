package middleware

import (
	"net/http"

	"bitbucket.org/idomdavis/gohttp/conversation"
	"bitbucket.org/idomdavis/gohttp/session"
)

// A RateLimiter is used to rate limit some aspect of an HTTP request. The
// ActionFunc returns a string describing the action which is then applied to
// the underlying limiter.
type RateLimiter struct {
	ActionFunc func(r *http.Request) string
	Limiter    session.Limiter

	conversation.Error
}

// Middleware returns the RateLimiter middleware function.
func (r *RateLimiter) Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		var action string

		if r.ActionFunc != nil {
			action = r.ActionFunc(req)
		}

		if action != "" && r.Limiter.Reject(action) {
			r.Handle(conversation.Respond(res, http.StatusTooManyRequests))
		} else {
			next.ServeHTTP(res, req)
		}
	})
}

// Source rate limiter which will limit requests from the same remote address.
func Source(r *http.Request) string {
	return r.RemoteAddr
}

// User rate limiter which will limit requests with the same basic auth user
// set.
func User(r *http.Request) string {
	user, _, _ := r.BasicAuth()
	return user
}
