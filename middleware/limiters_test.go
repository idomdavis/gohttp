package middleware_test

import "bitbucket.org/idomdavis/gohttp/middleware"

func ExampleSource() {
	_ = middleware.RateLimiter{ActionFunc: middleware.Source}
}

func ExampleUser() {
	_ = middleware.RateLimiter{ActionFunc: middleware.User}
}
