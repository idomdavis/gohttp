package conversation

// Error allows handling of multiple errors during a conversation by passing the
// errors onto a channel to be handled asynchronously. This allows conversations
// to be completed even if an action during the conversation fails.
type Error struct {
	// Channel can receive 0-n errors.
	Channel chan<- error
}

// Handle an error. If the errors is non-nil and a channel is registered with
// the handler then that error will be put onto the channel.
func (e Error) Handle(err error) {
	if e.Channel != nil && err != nil {
		e.Channel <- err
	}
}
