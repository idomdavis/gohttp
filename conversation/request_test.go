package conversation_test

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"bitbucket.org/idomdavis/gohttp/conversation"
)

func ExampleRequest_Get() {
	server := httptest.NewServer(http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {
			_ = conversation.Respond(w, http.StatusOK)
		}))

	response, err := http.Get(server.URL)

	if err != nil {
		fmt.Println(err)
	}

	_ = response.Body.Close()

	fmt.Println(conversation.Success(response))

	// Output:
	// true
}

func ExampleRequest_Post() {
	var body string

	server := httptest.NewServer(http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {
			var reply string
			_ = conversation.Unmarshal(r.Body, &reply)
			_ = conversation.Reply(w, reply)
		}))

	response, err := conversation.Request{}.Post(server.URL, `"body"`)

	if err != nil {
		fmt.Println(err)
	}

	_ = conversation.Unmarshal(response.Body, &body)
	_ = response.Body.Close()

	fmt.Println(body)

	// Output:
	// body
}

func ExampleRequest_Make() {
	var body string

	server := httptest.NewServer(http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {
			u, p, _ := r.BasicAuth()
			h := r.Header.Get("header")
			_ = conversation.Reply(w, "\""+h+u+p+"\"")
		}))

	response, err := conversation.Request{
		Username: "u",
		Password: "p",
		Headers:  map[string]string{"header": "h"},
	}.Make(http.MethodGet, server.URL, nil)

	if err != nil {
		fmt.Println(err)
	}

	_ = conversation.Unmarshal(response.Body, &body)
	_ = response.Body.Close()

	fmt.Println(body)

	// Output:
	// hup
}

func TestRequest_Make(t *testing.T) {
	t.Run("An invalid HTTP Method will error", func(t *testing.T) {
		r, err := conversation.Request{}.Make(" ", "http://localhost", nil)

		defer func() {
			if r != nil {
				_ = r.Close
			}
		}()

		expected := `gohttp: net/http: invalid method " "`

		switch {
		case err == nil:
			t.Error("Expected error for invalid HTTP method")
		case err.Error() != expected:
			t.Errorf("Expected %q, got: %v", expected, err)
		}
	})

	t.Run("An invalid URL will error", func(t *testing.T) {
		r, err := conversation.Request{}.Make(http.MethodGet, "", nil)

		defer func() {
			if r != nil {
				_ = r.Close
			}
		}()

		expected := `gohttp: Get "": unsupported protocol scheme ""`

		switch {
		case err == nil:
			t.Error("Expected error for invalid URL")
		case err.Error() != expected:
			t.Errorf("Expected %q, got: %v", expected, err)
		}
	})
}

func TestRequest_Post(t *testing.T) {
	t.Run("An invalid payload will error", func(t *testing.T) {
		r, err := conversation.Request{}.Post("http://localhost", make(chan int))

		defer func() {
			if r != nil {
				_ = r.Close
			}
		}()

		expected := "failed to marshal request payload"

		switch {
		case err == nil:
			t.Error("Expected error for invalid payload")
		case !strings.Contains(err.Error(), expected):
			t.Errorf("Expected error containing %q, got: %v", expected, err)
		}
	})
}
