package conversation_test

import (
	"errors"
	"fmt"

	"bitbucket.org/idomdavis/gohttp/conversation"
)

func ExampleError_Handle() {
	c := make(chan error)
	e := conversation.Error{Channel: c}

	go e.Handle(nil)
	go e.Handle(errors.New("error"))

	fmt.Println(<-c)

	// Output:
	// error
}
