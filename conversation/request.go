package conversation

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"
)

// Request allows for simple HTTP requests to be made to a URL. If a Username
// and Password is set then Basic Auth will be used. If no client is set then a
// default client is constructed using the DefaultTimeout.
type Request struct {
	// Username if Basic Auth is being used. Password must also be set.
	Username string

	// Password is Basic Auth is being used. Username must also be set.
	Password string

	// Headers to use on the request. If Basic Auth is being used then the
	// Authorisation header is set before the headers are applied.
	Headers map[string]string

	// Client used for the request. If no client is set then a default client
	// will be constructed.
	Client *http.Client
}

// DefaultTimeout used by clients if no client is defined on a Request.
const DefaultTimeout = time.Second * 10

// Make a request, using Basic Auth if Username and Password are set. A client
// will be constructed for the request is none is set.
func (r Request) Make(method, url string, payload []byte) (*http.Response, error) {
	if r.Client == nil {
		r.Client = &http.Client{Timeout: DefaultTimeout,
			Transport: http.DefaultTransport}
	}

	reader := bytes.NewReader(payload)
	req, err := http.NewRequestWithContext(
		context.Background(), method, url, reader)

	if err != nil {
		return nil, fmt.Errorf("gohttp: %w", err)
	}

	if r.Username != "" && r.Password != "" {
		req.SetBasicAuth(r.Username, r.Password)
	}

	for k, v := range r.Headers {
		req.Header.Set(k, v)
	}

	res, err := r.Client.Do(req)

	if err != nil {
		return nil, fmt.Errorf("gohttp: %w", err)
	}

	return res, nil
}

// Get the response from the given url. Get is a convenience method around Make
// which uses MethodGet and a nil body.
func (r Request) Get(url string) (*http.Response, error) {
	return r.Make(http.MethodGet, url, nil)
}

// Post the payload to the given url. The payload will be marshalled into JSON.
// Post is a convenience method around Make which uses MethodPost and the output
// of json.Marshal for the body.
func (r Request) Post(url string, payload interface{}) (*http.Response, error) {
	b, err := json.Marshal(payload)

	if err != nil {
		return nil, fmt.Errorf("failed to marshal request payload: %w", err)
	}

	return r.Make(http.MethodPost, url, b)
}
