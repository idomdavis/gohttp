// Package conversation deals with requests and responses while having an HTTP
// conversation. It provides convenience functions for dealing with common
// REST conversations.
package conversation
