package conversation

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
)

// ErrNilResponse is returned if an attempted is made to unmarshal a nil
// response.
var ErrNilResponse = errors.New("nil response")

// Success returns true if the HTTP Response type contains a success code (200,
// 201, 202).
func Success(r *http.Response) bool {
	if r == nil {
		return false
	}

	switch r.StatusCode {
	case http.StatusOK, http.StatusCreated, http.StatusAccepted:
		return true
	default:
		return false
	}
}

// Reply to the conversation with an HTTP OK and the given body. Bodies that are
// not of type []byte or string will be marshalled into JSON. If the marshall
// failed a 500 error is sent and an error returned. Zero length bodies will
// simply result in the default 200 status text.
func Reply(w http.ResponseWriter, body interface{}) error {
	var (
		content    []byte
		contentErr error
		writeErr   error
	)

	code := http.StatusOK

	switch body := body.(type) {
	case []byte:
		content = body
	case string:
		content = []byte(body)
	default:
		content, contentErr = json.Marshal(body)
	}

	if contentErr != nil {
		code = http.StatusInternalServerError
	}

	writeErr = Write(w, code, content)

	switch {
	case contentErr != nil && writeErr != nil:
		err := fmt.Errorf("failed to marshal HTTP body: %w", contentErr)
		return fmt.Errorf("%w, additionally: %s", err, writeErr.Error())
	case contentErr != nil:
		return fmt.Errorf("failed to marshal HTTP body: %w", contentErr)
	case writeErr != nil:
		return writeErr
	default:
		return nil
	}
}

// Respond using the default HTTP status text.
func Respond(w http.ResponseWriter, code int) error {
	return Write(w, code, []byte{})
}

// Write an HTTP response, adding some extra headers along the way. If no body
// is set then the default HTTP status text for that code will be used.
func Write(w http.ResponseWriter, code int, body []byte) error {
	if len(body) == 0 {
		body = []byte(http.StatusText(code))
	}

	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.Header().Set("X-Clacks-Overhead", "GNU Terry Pratchett, Russel Winder")
	w.WriteHeader(code)

	if _, err := w.Write(body); err != nil {
		return fmt.Errorf("failed to write HTTP response: %w", err)
	}

	return nil
}
