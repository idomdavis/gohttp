package conversation

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
)

// Unmarshal a JSON request or response into the given interface. The interface
// should be a pointer.
func Unmarshal(r io.Reader, in interface{}) error {
	if r == nil {
		return ErrNilResponse
	} else if b, err := ioutil.ReadAll(r); err != nil {
		return fmt.Errorf("failed to read body: %w", err)
	} else if err := json.Unmarshal(b, in); err != nil {
		return fmt.Errorf("failed to unmarshal body: %w", err)
	}

	return nil
}
