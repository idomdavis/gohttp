package conversation_test

import (
	"bytes"
	"errors"
	"fmt"
	"strings"
	"testing"

	"bitbucket.org/idomdavis/gohttp/conversation"
)

func ExampleUnmarshal() {
	var body string

	err := conversation.Unmarshal(bytes.NewReader([]byte(`"body"`)), &body)

	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(body)

	// Output:
	// body
}

func TestUnmarshal(t *testing.T) {
	t.Run("A nil reader will return an error", func(t *testing.T) {
		var body string

		err := conversation.Unmarshal(nil, &body)

		switch {
		case err == nil:
			t.Error("Expected error for nil reader")
		case errors.Is(err, conversation.ErrNilResponse):
			// expected
		default:
			t.Errorf("Unexptected error: %v", err)
		}
	})

	t.Run("An invalid body will error", func(t *testing.T) {
		var body string

		err := conversation.Unmarshal(bytes.NewReader([]byte("body")), &body)
		expected := "failed to unmarshal body"

		switch {
		case err == nil:
			t.Error("Expected error for invalid payload")
		case !strings.Contains(err.Error(), expected):
			t.Errorf("Expected error containing %q, got: %v", expected, err)
		}
	})

	t.Run("Reader errors will be reported", func(t *testing.T) {
		var body string

		err := conversation.Unmarshal(&errReader{}, &body)
		expected := "failed to read body"

		switch {
		case err == nil:
			t.Error("Expected error for invalid payload")
		case !strings.Contains(err.Error(), expected):
			t.Errorf("Unexpected error: %v", err)
		}
	})
}

type errReader struct{}

func (errReader) Read(_ []byte) (int, error) {
	return 0, errors.New("read failure")
}
