package conversation_test

import (
	"bytes"
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"bitbucket.org/idomdavis/gohttp/conversation"
)

func ExampleSuccess() {
	fmt.Println(conversation.Success(&http.Response{StatusCode: 200}))
	fmt.Println(conversation.Success(&http.Response{StatusCode: 400}))
	fmt.Println(conversation.Success(nil))

	// Output:
	// true
	// false
	// false
}

func ExampleWrite() {
	header := "X-Content-Type-Options"
	w := httptest.ResponseRecorder{Body: &bytes.Buffer{}}
	err := conversation.Write(&w, http.StatusOK, []byte("body"))

	if err != nil {
		fmt.Println(err)
	}

	fmt.Printf("%s: %s\n", header, w.Header().Get(header))
	fmt.Printf("%d - %s\n", w.Code, w.Body.String())

	// Output:
	// X-Content-Type-Options: nosniff
	// 200 - body
}

func ExampleReply() {
	w := httptest.ResponseRecorder{Body: &bytes.Buffer{}}

	if err := conversation.Reply(&w, []byte("body")); err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(w.Body.String())
	}

	w = httptest.ResponseRecorder{Body: &bytes.Buffer{}}

	if err := conversation.Reply(&w, "body"); err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(w.Body.String())
	}

	w = httptest.ResponseRecorder{Body: &bytes.Buffer{}}
	payload := struct{ Data string }{Data: "body"}

	if err := conversation.Reply(&w, payload); err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(w.Body.String())
	}

	// Output:
	// body
	// body
	// {"Data":"body"}
}

func TestWrite(t *testing.T) {
	t.Run("Write errors will be reported", func(t *testing.T) {
		err := conversation.Write(&responseErr{}, http.StatusOK, []byte("body"))

		expected := "failed to write HTTP response: write error"

		switch {
		case err == nil:
			t.Error("Expected error for invalid payload")
		case err.Error() != expected:
			t.Errorf("Expected %q, got: %v", expected, err)
		}
	})
}

func TestReply(t *testing.T) {
	t.Run("A content error will return 500", func(t *testing.T) {
		w := httptest.ResponseRecorder{Body: &bytes.Buffer{}}
		err := conversation.Reply(&w, make(chan int))
		expected := "failed to marshal HTTP body"

		switch {
		case err == nil:
			t.Error("Expected error for invalid payload")
		case !strings.Contains(err.Error(), expected):
			t.Errorf("Expected error containing %q, got: %v", expected, err)
		}

		if w.Code != http.StatusInternalServerError {
			t.Errorf("Expected 500, got %d", w.Code)
		}
	})

	t.Run("Write errors will be reported", func(t *testing.T) {
		err := conversation.Reply(&responseErr{}, `"body"`)

		expected := "failed to write HTTP response: write error"

		switch {
		case err == nil:
			t.Error("Expected write error")
		case err.Error() != expected:
			t.Errorf("Expected %q, got: %v", expected, err)
		}
	})

	t.Run("Total failure will be reported", func(t *testing.T) {
		err := conversation.Reply(&responseErr{}, make(chan int))

		expected := "failed to marshal HTTP body"
		additional := "additionally: failed to write"

		switch {
		case err == nil:
			t.Error("Expected error for invalid payload")
		case !strings.Contains(err.Error(), expected):
			t.Errorf("Expected error containing %q, got: %v", expected, err)
		case !strings.Contains(err.Error(), additional):
			t.Errorf("Expected error containing %q, got: %v", additional, err)
		}
	})
}

type responseErr struct {
	httptest.ResponseRecorder
}

func (responseErr) Write([]byte) (int, error) {
	return 0, errors.New("write error")
}
