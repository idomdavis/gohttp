package handler

import (
	"net/http"

	"bitbucket.org/idomdavis/gohttp/conversation"
)

// The Unauthorised handler will return a status Forbidden.
type Unauthorised struct {
	conversation.Error
}

// Handle the request by returning a status Forbidden.
func (u Unauthorised) Handle(w http.ResponseWriter, _ *http.Request) {
	u.Error.Handle(conversation.Respond(w, http.StatusForbidden))
}
