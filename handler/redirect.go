package handler

import "net/http"

// Redirect handler used to redirect a request.
type Redirect string

// Handle the request by redirecting to Redirect.
func (r Redirect) Handle(res http.ResponseWriter, req *http.Request) {
	http.Redirect(res, req, string(r), http.StatusSeeOther)
}
