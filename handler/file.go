package handler

import "net/http"

// File defines the path of a static file to be served by the server.
type File string

// Handler will serve the file set on this handler.
func (f File) Handler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, string(f))
}
