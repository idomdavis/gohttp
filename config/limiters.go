package config

import (
	"bitbucket.org/idomdavis/goconfigure"
)

const (
	source = "Source"
	user   = "User"
)

// Limiters in use by the system.
type Limiters struct {
	// Source limiter rate limits connections from the same source.
	Source *Limiter

	// User limiter rate limits connections from the same user.
	User *Limiter
}

// Configure the limiter.
func (l *Limiters) Configure(config *goconfigure.Settings) {
	l.Source = NewLimiter(source, 100)
	l.User = NewLimiter(user, 5)

	config.Add(l.Source)
	config.Add(l.User)
}
