package config_test

import (
	"fmt"

	"bitbucket.org/idomdavis/goconfigure"
	"bitbucket.org/idomdavis/gohttp/config"
)

func ExampleLimiters_Configure() {
	l := config.Limiters{}
	s := goconfigure.NewSettings("TLS")

	l.Configure(s)

	err := s.ParseUsing([]string{
		"--user-limiter-depth", "10", "--source-limiter-depth", "50",
	}, goconfigure.ConsoleReporter{})

	if err != nil {
		fmt.Println(err)
	}

	// Output:
	// Source Limiter Settings: [Depth:50, Limit:100, TTL:1s, Type:Source]
	// User Limiter Settings: [Depth:10, Limit:5, TTL:1s, Type:User]
}
