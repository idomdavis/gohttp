package config_test

import (
	"fmt"

	"bitbucket.org/idomdavis/goconfigure"
	"bitbucket.org/idomdavis/gohttp/config"
)

func ExampleTLS_Register() {
	tls := config.TLS{}
	s := goconfigure.NewSettings("TLS")
	s.Add(&tls)

	err := s.ParseUsing([]string{
		"-tls-key", "./tls.key", "--tls-certificate", "./tls.cert",
	}, goconfigure.ConsoleReporter{})

	if err != nil {
		fmt.Println(err)
	}

	// Output:
	// TLS settings: [TLS Certificate Path:./tls.cert, TLS Key Path:./tls.key]
}
