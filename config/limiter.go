package config

import (
	"fmt"
	"strings"
	"time"

	"bitbucket.org/idomdavis/goconfigure"
)

// Limiter configuration.
type Limiter struct {
	// Type of limiter
	Type string

	// Depth of the limiter (i.e. number of distinct items to hold)
	Depth int

	// Limit at which the limiter will trip (i.e. the number of time something
	// needs to be seen before the limiter trips)
	Limit int

	// TTL for the items in the limiter
	TTL time.Duration

	// DefaultLimit for this limiter
	DefaultLimit int
}

// NewLimiter returns a limiter of the specified type and default limit.
func NewLimiter(t string, d int) *Limiter {
	return &Limiter{Type: t, DefaultLimit: d}
}

// Description for the Limiter.
func (l *Limiter) Description() string {
	return fmt.Sprintf("%s Limiter Settings", l.Type)
}

// Register the limiter.
func (l *Limiter) Register(opts goconfigure.OptionSet) {
	t := strings.ToLower(l.Type)
	opts.Add(opts.Option(&l.Depth, 1000, fmt.Sprintf("%s-limiter-depth", t),
		"Depth of the limiter (i.e. number of distinct items to hold)"))
	opts.Add(opts.Option(&l.Limit, l.DefaultLimit, fmt.Sprintf("%s-limiter-limit", t),
		"Number of times something needs to be seen before the limiter trips"))
	opts.Add(opts.Option(&l.TTL, time.Second, fmt.Sprintf("%s-limiter-ttl", t),
		"Time-to-live for items in the limiter"))
}

// Data for the Limiter.
func (l *Limiter) Data() interface{} {
	return struct {
		Type  string
		Depth string
		Limit string
		TTL   string
	}{
		Type:  goconfigure.Sanitise(l.Type, l.Type, goconfigure.UNSET),
		Depth: goconfigure.Sanitise(l.Depth, l.Depth, goconfigure.UNSET),
		Limit: goconfigure.Sanitise(l.Limit, l.Limit, goconfigure.UNSET),
		TTL:   goconfigure.Sanitise(l.TTL, l.TTL, goconfigure.UNSET),
	}
}
