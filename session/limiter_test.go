package session_test

import (
	"fmt"
	"time"

	"bitbucket.org/idomdavis/gohttp/session"
)

func ExampleLimiter_Reject() {
	l := session.Limiter{Limit: 2}

	fmt.Println(l.Reject("action"))
	fmt.Println(l.Reject("action"))
	fmt.Println(l.Reject("action"))

	// Output:
	// false
	// false
	// true
}

func ExampleLimiter_Reject_defaults() {
	l := session.Limiter{}

	// We don't expect to see these rejected as they're within the limit
	for i := 0; i < session.DefaultLimit; i++ {
		if l.Reject("action") {
			fmt.Println("rejected!")
		}
	}

	// This will be rejected as it takes us past the limit
	fmt.Println(l.Reject("action"))

	// Output:
	// true
}

func ExampleLimiter_Reject_overflow() {
	l := session.Limiter{Size: 1, Limit: 1}

	fmt.Println(l.Reject("action 1"))

	// Action 2 forces action 1 out of the limiter
	fmt.Println(l.Reject("action 2"))
	// Action 2 is now limited on its second action
	fmt.Println(l.Reject("action 2"))
	// Action 1 is now treated as a new action, pushing action 2 out
	fmt.Println(l.Reject("action 1"))
	// Action 2 is now a new action
	fmt.Println(l.Reject("action 2"))

	// Output:
	// false
	// false
	// true
	// false
	// false
}

func ExampleLimiter_Reject_timeout() {
	l := session.Limiter{Window: time.Nanosecond, Size: 1}

	fmt.Println(l.Reject("action"))
	time.Sleep(time.Nanosecond)
	// Second action is not limited as the first action has expired
	fmt.Println(l.Reject("action"))

	// Output:
	// false
	// false
}
