package session_test

import (
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/dgrijalva/jwt-go"

	"bitbucket.org/idomdavis/gohttp/session"
)

func ExampleJWTSignatory_Sign() {
	w := httptest.ResponseRecorder{}
	ctx := session.Context{"k": "v"}

	err := session.JWTSignatory{}.Sign(&w, ctx)

	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(w.Header().Get(session.AuthHeader)[:len(session.TokenPrefix)])

	// Output:
	// Bearer
}

func ExampleJWTSignatory_Validate() {
	r := http.Request{Header: map[string][]string{}}
	w := httptest.ResponseRecorder{}
	sig := session.JWTSignatory{}

	_ = sig.Sign(&w, session.Context{"k": "v"})

	r.Header.Set(session.AuthHeader, w.Header().Get(session.AuthHeader))

	ctx, err := sig.Validate(&r)

	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(ctx)

	// Output:
	// map[k:v]
}

func TestJWTSignatory_Sign(t *testing.T) {
	t.Run("Unsupported signing methods will error", func(t *testing.T) {
		w := httptest.ResponseRecorder{}
		sig := session.JWTSignatory{Method: jwt.SigningMethodRS512}
		err := sig.Sign(&w, session.Context{})

		expected := "failed to sign JWT: key is invalid"

		switch {
		case err == nil:
			t.Error("Expected error for unsupported signing method")
		case err.Error() != expected:
			t.Errorf("Expected %q, got: %v", expected, err)
		}
	})
}

func TestJWTSignatory_Validate(t *testing.T) {
	t.Run("An invalid token will not validate", func(t *testing.T) {
		r := http.Request{Header: map[string][]string{
			session.AuthHeader: {"invalid"},
		}}

		ctx, err := session.JWTSignatory{}.Validate(&r)

		expected :=
			"failed to parse token: token contains an invalid number of segments"

		switch {
		case err == nil:
			t.Error("Expected error for invalid token")
		case err.Error() != expected:
			t.Errorf("Expected %q, got: %v", expected, err)
		case len(ctx) != 0:
			t.Errorf("Expected empty context")
		}
	})

	t.Run("An empty token will error", func(t *testing.T) {
		r := http.Request{Header: map[string][]string{}}
		ctx, err := session.JWTSignatory{}.Validate(&r)

		switch {
		case err == nil:
			t.Error("Expected error for empty token")
		case !errors.Is(err, session.ErrNoCredentialsFound):
			t.Errorf("Expected %v, got: %v", session.ErrNoCredentialsFound, err)
		case len(ctx) != 0:
			t.Errorf("Expected empty context")
		}
	})

	t.Run("Expired tokens will not validate", func(t *testing.T) {
		// Expiry is done to the nearest second, so rather than generating a
		// token on the fly and then waiting here's one we generated earlier.
		token := "Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9." +
			"eyJleHAiOjE1OTQzNjQ2NDYsImsiOiJ2In0." +
			"5UGKNseilB2SdP1Z2IIRna4iKpT3WNSgQMxKuOjAsbTMBKrNJ" +
			"xH4gkvOgX7L5jv5_vnLlrlr7M7Kqbjri-rWRg"
		r := http.Request{Header: map[string][]string{}}

		r.Header.Set(session.AuthHeader, token)

		ctx, err := session.JWTSignatory{}.Validate(&r)
		expected := "failed to parse token: Token is expired"

		switch {
		case err == nil:
			t.Error("Expected error for expired token")
		case err.Error() != expected:
			t.Errorf("Expected %q, got: %v", expected, err)
		case len(ctx) != 0:
			t.Errorf("Expected empty context")
		}
	})
}
