// Package session handles session management via secure cookies or JWT tokens.
// It can work in conjunction with the Authenticator middleware to ensure calls
// within sessions are properly authenticated.
package session
