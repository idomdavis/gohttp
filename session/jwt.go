package session

import (
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
)

// JWTSignatory is a signatory that stores claims in a JWT. While the blank
// signatory can be used it is advisable to set a Secret to properly secure the
// token.
type JWTSignatory struct {
	Secret string
	TTL    time.Duration
	Method jwt.SigningMethod
}

// DefaultSigningMethod is used by a JWT token if no signing method is set.
var DefaultSigningMethod = jwt.SigningMethodHS512

// ErrNoCredentialsFound is returned if an attempt is made to validate an empty
// bearer token.
var ErrNoCredentialsFound = errors.New("no credentials found")

const (
	// AuthHeader is the header used to store the bearer token.
	AuthHeader = "Authorization"

	// TokenPrefix is applied/removed from the front of the JWT token.
	TokenPrefix = "Bearer "
)

const expiryClaim = "exp"

// Sign a response with the given Context. The Authorisation header is set on
// the response.
func (j JWTSignatory) Sign(w http.ResponseWriter, ctx Context) error {
	if j.TTL == 0 {
		j.TTL = DefaultTTL
	}

	if j.Method == nil {
		j.Method = DefaultSigningMethod
	}

	claims := jwt.MapClaims{}

	for k, v := range ctx {
		claims[k] = v
	}

	claims[expiryClaim] = time.Now().Add(j.TTL).Unix()

	token := jwt.NewWithClaims(j.Method, claims)
	tokenString, err := token.SignedString([]byte(j.Secret))

	if err != nil {
		return fmt.Errorf("failed to sign JWT: %w", err)
	}

	w.Header().Set(AuthHeader, TokenPrefix+tokenString)

	return nil
}

// Validate a request by checking the Authorisation header. An empty Context is
// returned if there is an error validating the session.
func (j JWTSignatory) Validate(r *http.Request) (Context, error) {
	tokenString := strings.TrimPrefix(r.Header.Get(AuthHeader), TokenPrefix)

	if strings.TrimSpace(tokenString) == "" {
		return Context{}, ErrNoCredentialsFound
	}

	token, err := jwt.Parse(tokenString, j.keyFunc)

	if err != nil {
		return Context{}, fmt.Errorf("failed to parse token: %w", err)
	}

	return j.retrieveContext(token), nil
}

func (j JWTSignatory) keyFunc(_ *jwt.Token) (interface{}, error) {
	return []byte(j.Secret), nil
}

func (JWTSignatory) retrieveContext(token *jwt.Token) Context {
	// Not entirely sure how it's possible to get here without MapClaims, and
	// all attempts to stuff odd things into tokens have lead to either this
	// not failing, or validation failing before we get here. Since we don't
	// want an unexpected panic due to "this shouldn't happen" (famous last
	// words) we'll treat non map claims as being empty and ignore any cast
	// failure.
	claims, _ := token.Claims.(jwt.MapClaims)
	ctx := make(Context, len(claims))

	for k, v := range claims {
		if value, ok := v.(string); ok {
			ctx[k] = value
		}
	}

	return ctx
}
