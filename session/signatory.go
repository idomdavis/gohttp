package session

import (
	"net/http"
	"time"
)

// A Signatory is used to sign and validate sessions.
type Signatory interface {
	// Sign a session with the given Context. A valid response will be written
	// as part of this.
	Sign(w http.ResponseWriter, ctx Context) error

	// Validate a request, returning a Context if the request is validated, or
	// and error otherwise.
	Validate(r *http.Request) (Context, error)
}

// DefaultTTL for signatory implementations, causing the session to expire after
// 12 hours.
const DefaultTTL = time.Hour * 12
