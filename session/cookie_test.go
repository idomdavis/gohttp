package session_test

import (
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gorilla/securecookie"

	"bitbucket.org/idomdavis/gohttp/session"
)

func ExampleCookieSignatory_Sign() {
	codec := securecookie.New(make([]byte, 64), make([]byte, 32))
	w := httptest.ResponseRecorder{}
	ctx := session.Context{"k": "v"}
	err := session.CookieSignatory{Codec: codec}.Sign(&w, ctx)

	if err != nil {
		fmt.Println(err)
	}

	// Output:
	//
}

func ExampleCookieSignatory_Validate() {
	codec := securecookie.New(make([]byte, 64), make([]byte, 32))
	r := http.Request{Header: map[string][]string{}}
	w := httptest.ResponseRecorder{}
	sig := session.CookieSignatory{Codec: codec}

	_ = sig.Sign(&w, session.Context{"k": "v"})

	r.Header.Set("Cookie", w.Header().Get("Set-Cookie"))

	ctx, err := sig.Validate(&r)

	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(ctx)

	// Output:
	// map[k:v]
}

func ExampleCookieSignatory_Clear() {
	w := httptest.ResponseRecorder{}
	session.CookieSignatory{}.Clear(&w)

	fmt.Println(w.Header().Get("Set-Cookie"))

	// Output:
	// cookie=; Path=/; Max-Age=0; HttpOnly; Secure
}

func TestCookieSignatory_Sign(t *testing.T) {
	t.Run("Not setting an Codec will error", func(t *testing.T) {
		w := httptest.ResponseRecorder{}
		err := session.CookieSignatory{}.Sign(&w, session.Context{})

		switch {
		case err == nil:
			t.Error("Expected error with no encoder/decoder")
		case !errors.Is(err, session.ErrNoCodec):
			t.Errorf("Expected %v, got: %v", session.ErrNoCodec, err)
		}
	})

	t.Run("Invalid key lengths will error", func(t *testing.T) {
		codec := securecookie.New(make([]byte, 1), make([]byte, 1))
		w := httptest.ResponseRecorder{}
		ctx := session.Context{"k": "v"}
		err := session.CookieSignatory{Codec: codec}.Sign(&w, ctx)

		expected := "failed to encode cookie: securecookie: error - caused by: " +
			"crypto/aes: invalid key size 1"

		switch {
		case err == nil:
			t.Error("Expected error with invalid key lengths")
		case err.Error() != expected:
			t.Errorf("Expected %q, got: %v", expected, err)
		}
	})
}

func TestCookieSignatory_Validate(t *testing.T) {
	t.Run("Not setting an Codec will error", func(t *testing.T) {
		ctx, err := session.CookieSignatory{}.Validate(&http.Request{})

		switch {
		case err == nil:
			t.Error("Expected error with no encoder/decoder")
		case !errors.Is(err, session.ErrNoCodec):
			t.Errorf("Expected %v, got: %v", session.ErrNoCodec, err)
		case len(ctx) != 0:
			t.Errorf("Expected empty context")
		}
	})

	t.Run("Not setting a cookie will error", func(t *testing.T) {
		codec := securecookie.New(make([]byte, 64), make([]byte, 32))
		sig := session.CookieSignatory{Codec: codec}
		ctx, err := sig.Validate(&http.Request{})

		expected := "failed to retrieve cookie: http: named cookie not present"

		switch {
		case err == nil:
			t.Error("Expected error for no cookie")
		case err.Error() != expected:
			t.Errorf("Expected %q, got: %v", expected, err)
		case len(ctx) != 0:
			t.Errorf("Expected empty context")
		}
	})

	t.Run("An expired cookie will error", func(t *testing.T) {
		codec := securecookie.New(make([]byte, 64), make([]byte, 32))
		codec.MaxAge(1)
		sig := session.CookieSignatory{Codec: codec}

		r := http.Request{Header: map[string][]string{}}

		r.Header.Set("Cookie", "cookie="+
			"MTU5NDM2NzY1OXxUM0NPSDBCXzFBMnQ0MHVIU3RtcC16X3lkaDFsbWdkZng2bUpHb"+
			"3Z0V29QX2R4SE1haVhmbzZXR2RycEZsbE9tMnc9PXyfFVLSMjeA9swIaCiDUAYhLB"+
			"jFh3S_NdFuGjSYxcdjvw==; "+
			"Path=/; Expires=Fri, 10 Jul 2020 07:54:29 GMT; "+
			"Max-Age=10; HttpOnly; Secure")

		ctx, err := sig.Validate(&r)

		expected := "failed to decode cookie: securecookie: expired timestamp"

		switch {
		case err == nil:
			t.Error("Expected error for expired cookie")
		case err.Error() != expected:
			t.Errorf("Expected %q, got: %v", expected, err)
		case len(ctx) != 0:
			t.Errorf("Expected empty context")
		}
	})
}
