package session

import (
	"errors"
	"fmt"
	"net/http"
	"time"
)

// Codec provides an interface to encode and decode a names value to and from a
// cookie string.
type Codec interface {
	// Encode the named value. The resulting string can be used in a cookie.
	Encode(name string, value interface{}) (string, error)

	// Decode the named cookie string into the given interface.
	Decode(name, value string, in interface{}) error
}

// CookieSignatory is a signatory that stores claims in a secure cookie. Name,
// Path and TTL all have sensible defaults, however, a valid Codec must
// be set. It is likely the codec will not use the TTL information set on the
// cookie given this can be manipulated, so the TTL may need to be correctly
// set on the codec as well as the CookieSignatory.
type CookieSignatory struct {
	Name string
	Path string

	Secure bool

	TTL time.Duration

	Codec
}

// ErrNoCodec is returned if a CookieSignatory is used without an codec being
// set.
var ErrNoCodec = errors.New("no encoder/decoder defined")

const (
	defaultName = "cookie"
	defaultPath = "/"
)

// Sign and drop a cookie on the response. If an error is returned then no
// cookie will have been dropped.
func (c CookieSignatory) Sign(w http.ResponseWriter, ctx Context) error {
	if c.Codec == nil {
		return ErrNoCodec
	}

	if c.Name == "" {
		c.Name = defaultName
	}

	if c.Path == "" {
		c.Path = defaultPath
	}

	if c.TTL == 0 {
		c.TTL = DefaultTTL
	}

	encoded, err := c.Encode(c.Name, ctx)

	if err != nil {
		return fmt.Errorf("failed to encode cookie: %w", err)
	}

	http.SetCookie(w, &http.Cookie{
		Name:     c.Name,
		Value:    encoded,
		Path:     c.Path,
		Secure:   c.Secure,
		HttpOnly: true,
		Expires:  time.Now().Add(c.TTL),
		MaxAge:   int(c.TTL / time.Second),
	})

	return nil
}

// Validate a request, returning the context for the request. If there is no
// valid context on the request then an empty Context is returned. An error will
// always return an empty Context.
func (c CookieSignatory) Validate(r *http.Request) (Context, error) {
	var ctx Context

	if c.Codec == nil {
		return ctx, ErrNoCodec
	}

	if c.Name == "" {
		c.Name = defaultName
	}

	cookie, err := r.Cookie(c.Name)

	if err != nil {
		return ctx, fmt.Errorf("failed to retrieve cookie: %w", err)
	}

	err = c.Decode(c.Name, cookie.Value, &ctx)

	if err != nil {
		return ctx, fmt.Errorf("failed to decode cookie: %w", err)
	}

	return ctx, nil
}

// Clear the current cookie. An empty cookie set to expire immediately is set
// in place of any session cookie.
func (c CookieSignatory) Clear(w http.ResponseWriter) {
	if c.Name == "" {
		c.Name = defaultName
	}

	if c.Path == "" {
		c.Path = defaultPath
	}

	http.SetCookie(w, &http.Cookie{
		Name:     c.Name,
		Value:    "",
		Path:     c.Path,
		Secure:   true,
		HttpOnly: true,
		MaxAge:   -1,
	})
}
