package session

import (
	"net/http"
	"sync"
)

// Context stores session claims encoded in the JWT or secure cookie.
type Context map[string]string

var (
	mutex    sync.RWMutex
	contexts = map[*http.Request]Context{}
)

// Store a context for the given request in the session cache.
func Store(r *http.Request, ctx Context) {
	mutex.Lock()
	contexts[r] = ctx
	mutex.Unlock()
}

// Get a context for the given request from the session cache. If no context
// exists an empty context is returned.
func Get(r *http.Request) Context {
	mutex.RLock()
	ctx := contexts[r]
	mutex.RUnlock()

	return ctx
}

// Delete the context for the given request from the session cache.
func Delete(r *http.Request) {
	mutex.Lock()
	delete(contexts, r)
	mutex.Unlock()
}
