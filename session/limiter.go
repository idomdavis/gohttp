package session

import (
	"container/list"
	"sync"
	"time"
)

// A Limiter is used to limit an action, defined by a string describing that
// action. An action will fail a limit check if the limiter has seen the action
// string more than Limit times within the limiter Window. Limiters have
// sensible defaults if no limit values are set.
//
// A Limiter will use at most approximately Size * Limit * 60bytes, however,
// actual memory usage is likely to be lower depending on the number of actions
// being handled.
type Limiter struct {
	// Size of the limiter, in terms of how many distinct actions can be stored.
	// If adding an action takes the limiter over its defined size then the
	// least recently used action is removed. A size of less than 1 will result
	// in DefaultSize being used.
	Size int

	// Limit for the limiter. If an action string is seen more than Limit times
	// in Window the limiter will fail a check for the action. A Limit of less
	// than 1 will result in DefaultLimit being used.
	Limit int

	// Window of time the Limiter checks. Actions seen outside of the window
	// will not count towards the limit check. A Window of less than 1 will
	// result in DefaultWindow being used.
	Window time.Duration

	mutex  sync.Mutex
	index  *list.List
	values map[string]*list.Element
}

type hit struct{}

type data struct {
	value string
	hits  map[time.Time]hit
}

const (
	// DefaultSize of a limiter of 1000 distinct actions.
	DefaultSize = 1000

	// DefaultLimit on an action which will trip the limiter if an action is
	// seen more than 10 times in the limiters window.
	DefaultLimit = 10

	// DefaultWindow for the limiter of a second.
	DefaultWindow = time.Second
)

// Reject return true if the given action should be rejected due to rate limits.
// Any expired hits for the action are pruned. If adding this action causes the
// backing cache to exceed its maximum size the least recently used item is
// ejected.
func (l *Limiter) Reject(action string) bool {
	l.init()
	l.mutex.Lock()

	e, ok := l.values[action]

	l.mutex.Unlock()

	if !ok {
		l.push(action)
		return false
	}

	return l.check(e)
}

// Push a new value to the backing cache, dropping the last entry in the cache
// if we're bigger than the maximum allowed size.
func (l *Limiter) push(action string) {
	d := data{
		value: action,
		hits:  map[time.Time]hit{time.Now().Add(l.Window): {}},
	}

	l.mutex.Lock()

	l.values[action] = l.index.PushFront(d)

	if l.index.Len() > l.Size {
		e := l.index.Back()
		v := e.Value.(data)
		l.index.Remove(e)
		delete(l.values, v.value)
	}

	l.mutex.Unlock()
}

// Check the list element to see if the value it represents should be limited.
// During the check all expired hits will be removed and a new hit added. The
// element will also be moved to the front of the cache since it is the most
// recently active.
func (l *Limiter) check(e *list.Element) bool {
	var limited bool

	now := time.Now()
	eldest := now.Add(l.Window)

	v := e.Value.(data)
	v.hits[now.Add(l.Window)] = hit{}

	for k := range v.hits {
		if now.After(k) {
			delete(v.hits, k)
		} else if eldest.After(k) {
			eldest = k
		}
	}

	if len(v.hits) > l.Limit {
		delete(v.hits, eldest)

		limited = true
	}

	e.Value = v

	l.mutex.Lock()
	l.index.MoveToFront(e)
	l.mutex.Unlock()

	return limited
}

func (l *Limiter) init() {
	if l.Size < 1 {
		l.Size = DefaultSize
	}

	if l.Limit < 1 {
		l.Limit = DefaultLimit
	}

	if l.Window < 1 {
		l.Window = DefaultWindow
	}

	if l.index == nil {
		l.index = list.New()
	}

	if l.values == nil {
		l.values = map[string]*list.Element{}
	}
}
