package session_test

import (
	"fmt"
	"net/http"

	"bitbucket.org/idomdavis/gohttp/session"
)

func ExampleStore() {
	r := &http.Request{}
	ctx := session.Context{"key": "value"}

	session.Store(r, ctx)
	fmt.Println(session.Get(r))

	// Output:
	// map[key:value]
}

func ExampleDelete() {
	r := &http.Request{}
	ctx := session.Context{"key": "value"}

	session.Store(r, ctx)
	session.Delete(r)
	fmt.Println(session.Get(r))

	// Output:
	// map[]
}
