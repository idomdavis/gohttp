# gohttp

[![Build](https://img.shields.io/bitbucket/pipelines/idomdavis/gohttp?style=plastic)](https://bitbucket.org/idomdavis/gohttp/addon/pipelines/home)
[![Issues](https://img.shields.io/bitbucket/issues-raw/idomdavis/gohttp?style=plastic)](https://bitbucket.org/idomdavis/gohttp/issues)
[![Pull Requests](https://img.shields.io/bitbucket/pr-raw/idomdavis/gohttp?style=plastic)](https://bitbucket.org/idomdavis/gohttp/pull-requests/)
[![Go Doc](http://img.shields.io/badge/godoc-reference-5272B4.svg?style=plastic)](http://godoc.org/github.com/idomdavis/gohttp)
[![License](https://img.shields.io/badge/license-MIT-green?style=plastic)](https://opensource.org/licenses/MIT)

