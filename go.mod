module bitbucket.org/idomdavis/gohttp

go 1.14

require (
	bitbucket.org/idomdavis/goconfigure v0.5.9
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/securecookie v1.1.1
	github.com/sirupsen/logrus v1.8.1 // indirect
	github.com/stretchr/testify v1.5.1 // indirect
	golang.org/x/sys v0.0.0-20210521203332-0cec03c779c1 // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
